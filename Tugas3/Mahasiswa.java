package Tugas3;

import java.util.ArrayList;

public class Mahasiswa {
    
    private String nimMahasiswa;
    private String namaMahasiswa;
    private ArrayList<MataKuliah> mataKuliahs = new ArrayList<>();
    
    public ArrayList<MataKuliah> geMataKuliahs() {
        return mataKuliahs;
    }

    public void seMataKuliahs(ArrayList<MataKuliah> mataKuliahs) {
        this.mataKuliahs = mataKuliahs;
    }

    public String getNimMahasiswa() {
        return nimMahasiswa;
    }

    public void setNimMahasiswa(String nimMahasiswa) {
        this.nimMahasiswa = nimMahasiswa;
    }

    public String getNamaMahasiswa() {
        return namaMahasiswa;
    }

    public void setNamaMahasiswa(String namaMahasiswa) {
        this.namaMahasiswa = namaMahasiswa;
    }
}

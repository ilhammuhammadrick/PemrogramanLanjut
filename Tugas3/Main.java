package Tugas3;

import java.util.ArrayList;
import java.util.Scanner;

// Ilham Muhammad Rickwanto
// 235150400111003
// Sistem Informasi - A
public class Main {
    public static void main(String[] args) {
        ArrayList<Mahasiswa> mahasiswas = new ArrayList<>();

        Scanner input = new Scanner(System.in);
        boolean nextMahasiswa = true;
        while (nextMahasiswa) {
            System.out.print("Masukkan NIM: ");
            String nim = input.nextLine();

            System.out.print("Masukkan Nama: ");
            String nama = input.nextLine();

            Mahasiswa mahasiswa = new Mahasiswa();
            mahasiswa.setNamaMahasiswa(nama);
            mahasiswa.setNimMahasiswa(nim);

            ArrayList<MataKuliah> mataKuliahs = new ArrayList<>();
            boolean nextMataKuliah = true;
            while (nextMataKuliah) {
                System.out.print("Masukkan Kode Mata Kuliah: ");
                String kodeMataKuliah = input.nextLine();

                System.out.print("Masukkan Nama Mata Kuliah: ");
                String namaMataKuliah = input.nextLine();

                System.out.print("Masukkan Nilai Mata Kuliah: ");
                float nilaiMataKuliah = input.nextFloat();

                MataKuliah mataKuliah = new MataKuliah();
                mataKuliah.setKodeMataKuliah(kodeMataKuliah);
                mataKuliah.setNamaMataKuliah(namaMataKuliah);
                mataKuliah.setNilaiAngka(nilaiMataKuliah);
                mataKuliahs.add(mataKuliah);
                input.nextLine();
                System.out.print("Tambah Mata Kuliah? (Ketik \"y\" jika Ya, ketik \"t\" jika Tidak): ");
                String tambahMataKuliah = input.nextLine();

                if (tambahMataKuliah.equals("t")) {
                    nextMataKuliah = false;
                }
            }

            mahasiswa.seMataKuliahs(mataKuliahs);
            mahasiswas.add(mahasiswa);
            System.out.print("Tambah Mahasiswa? (Ketik \"y\" jika Ya, ketik \"t\" jika Tidak): ");
            String tambahMahasiswa = input.nextLine();

            if (tambahMahasiswa.equals("t")) {
                nextMahasiswa = false;
            }
        }

        for (int i = 0; i < mahasiswas.size(); i++) {
            System.out.println("======= Kartu Hasil Studi =======");
            Mahasiswa mahasiswa = (Mahasiswa) mahasiswas.get(i);
            System.out.println("Nama: " + mahasiswa.getNamaMahasiswa());
            System.out.println("NIM: " + mahasiswa.getNimMahasiswa());
            System.out.println("__________________________________________________________________________________________________________________");
            ArrayList<MataKuliah> mataKuliahs = mahasiswa.geMataKuliahs();
            for (int j = 0; j < mataKuliahs.size(); j++ ) {
                MataKuliah mtKuliah = (MataKuliah) mataKuliahs.get(j);
                System.out.println("Kode Mata Kuliah: " + mtKuliah.getKodeMataKuliah() + " | Nama Mata Kuliah: " + mtKuliah.getNamaMataKuliah() + " | Nilai: " + mtKuliah.getNilaiHuruf());
            }
            System.out.println("__________________________________________________________________________________________________________________");
            System.out.println();
            System.out.println();
        }
        input.close();
    }
}
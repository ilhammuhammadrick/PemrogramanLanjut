package Tugas3;

public class MataKuliah {

    private String kodeMataKuliah;
    private String namaMataKuliah;
    private float nilaiAngka;
    private char nilaiHuruf;

    public String getKodeMataKuliah() {
        return kodeMataKuliah;
    }

    public String getNamaMataKuliah() {
        return namaMataKuliah;
    }

    public void setNamaMataKuliah(String namaMataKuliah) {
        this.namaMataKuliah = namaMataKuliah;
    }

    public char getNilaiHuruf() {
        int temp = (int)nilaiAngka;
        if (temp > 80) {
            nilaiHuruf = 'A';
        }
        else if (temp > 60) {
            nilaiHuruf = 'B';
        }
        else if (temp > 50) {
            nilaiHuruf = 'C';
        }
        else if (temp > 40) {
            nilaiHuruf = 'D';
        }
        else {
            nilaiHuruf = 'E';
        }
        return nilaiHuruf;
    }

    public void setNilaiAngka(float nilaiAngka) {
        this.nilaiAngka = nilaiAngka;
    }

    public void setKodeMataKuliah(String kodeMataKuliah) {
        this.kodeMataKuliah = kodeMataKuliah;
    }
}